<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Enums\StatesEnum;
use App\Enums\CountriesCodesAndRegexEnum;

class Customer extends Model {

    protected $table = 'customer';
    protected $appends = array('phone_number', 'code', 'validity', 'country');

    public function getPhoneNumberAttribute() {
        $phone = explode(' ', $this->phone);
        return $phone[1];
    }

    public function getCodeAttribute() {
        $phone = explode(' ', $this->phone);
        return str_replace(array('(', ')'), '', $phone[0]);
    }

    public function getIsValidAttribute() {
        $number_validity = preg_match('/' . CountriesCodesAndRegexEnum::COUNTRIES_REGEX[$this->country] . '/', $this->phone);
        return $number_validity;
    }

    public function getValidityAttribute() {
        return StatesEnum::STATES_MAPPING[$this->is_valid];
    }

    public function getCountryAttribute() {
        return array_search($this->code, CountriesCodesAndRegexEnum::COUNTRIES_CODES);
    }

}
