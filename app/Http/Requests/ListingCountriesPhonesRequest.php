<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\CountriesCodesAndRegexEnum;
use App\Enums\StatesEnum;

class ListingCountriesPhonesRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'country' => ['nullable', 'string', 'in:' . implode(",", ['all' => 'all'] + CountriesCodesAndRegexEnum::COUNTRIES_LIST)],
            'state' => ['nullable', 'in:' . implode(",", ['all' => 'all'] + StatesEnum::STATES_LIST)]
        ];
    }

}
