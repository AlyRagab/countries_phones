<?php

namespace App\Http\Controllers;

use App\Http\Requests\ListingCountriesPhonesRequest;
use App\Services\ListingCountriesPhonesService;
use App\Services\PreparingListFormService;
use Illuminate\Support\Facades\Log;

/**
 * Description of CountriesPhonesController
 *
 * @author ali
 */
class CountriesPhonesController extends Controller {

    /**
     * Returns a list of countries phones with its state
     * 
     * @param ListingCountriesPhonesRequest $request Request Validation class
     * @param ListingCountriesPhonesService $data Listing service data
     * @param PreparingListFormService $form Form inputs
     */
    public function __invoke(
            ListingCountriesPhonesRequest $request,
            ListingCountriesPhonesService $data,
            PreparingListFormService $form
    ) {
        $form_data = $form->execute();
        //add the main service logic to try & catch handler
        try {
            $phones = paginate($data->execute($request->all()));
            return view('countries_phones', compact('phones', 'form_data'));
        } catch (\Exception $e) {
            //log the error
            Log::error($e->getMessage());
            //return error page
            return view('custom.error');
        }
    }

}
