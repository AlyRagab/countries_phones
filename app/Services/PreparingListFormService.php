<?php

namespace App\Services;

use App\Enums\StatesEnum;
use App\Enums\CountriesCodesAndRegexEnum;

/**
 * Description of PreparingListFormService
 *
 * @author ali
 */
class PreparingListFormService {

    /**
     * 
     * @return array
     */
    public function execute(): array {
        return [
            'states' => ['all' => 'All'] + StatesEnum::STATES_MAPPING,
            'countries' => ['all' => 'All'] + $this->formatCountriesSelections()
        ];
    }

    /**
     * 
     * @return array
     */
    private function formatCountriesSelections(): array {
        $list = [];
        foreach (CountriesCodesAndRegexEnum::COUNTRIES_LIST as $country) {
            $list[$country] = $country;
        }
        return $list;
    }

}
