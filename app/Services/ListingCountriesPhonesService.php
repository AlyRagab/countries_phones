<?php

namespace App\Services;

use App\Repositories\CustomersRepository;
use App\Enums\CountriesCodesAndRegexEnum;
use Illuminate\Support\Collection;

/**
 * Description of ListingCountriesPhonesService
 *
 * @author ali
 */
class ListingCountriesPhonesService {

    /**
     * 
     * @var CustomersRepository
     */
    private $repo = null;

    /**
     * 
     * @param CustomersRepository $repo
     */
    public function __construct(CustomersRepository $repo) {
        $this->repo = $repo;
    }

    /**
     * 
     * @param array $params the sent params
     * @return Collection the returned data
     */
    public function execute(array $params): Collection {
        //initiate each default params with (all) value if not sent
        if (!isset($params['country'])) {
            $params['country'] = 'all';
        }

        if (!isset($params['state'])) {
            $params['state'] = 'all';
        }
        //call get data with the existed params
        return $this->getData($params);
    }

    /**
     * 
     * @param array $params
     * @return Collection
     */
    private function getData(array $params): Collection {
        //check if country filter is sent with all, so return all the customers
        //then start to map the data
        if ($params['country'] == 'all') {
            $customers = $this->repo->all();
        } else {
            //if country filter has a specified country value, so start querying the db by repository
            $customers = $this->repo->
                    getNumbersByCountryCode(
                    CountriesCodesAndRegexEnum::COUNTRIES_CODES[$params['country']]
            );
        }
        //start to filter the result with sent state param and return the final result
        return $this->filterDataWithSentState($customers, $params['state']);
    }

    /**
     * Filter the returned data with sent state
     * 
     * @param Collection $customers
     * @param string $state the sent state
     * @return Collection
     */
    private function filterDataWithSentState(Collection $customers, string $state): Collection {
        //check if state param has a [not_valid,valid] value to filter the result
        if (is_numeric($state)) {
            $customers = $customers->where('is_valid', $state);
        }
        //return the final customer result
        return $customers;
    }

}
