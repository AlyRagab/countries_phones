<?php

namespace App\Repositories;

use App\Models\Customer;

/**
 * Description of CustomersRepository
 *
 * @author ali
 */
class CustomersRepository extends BaseRepository {

    public function __construct(Customer $model) {
        parent::__construct($model);
    }
    /**
     * 
     * @param string $code
     * @return \Illuminate\Support\Collection
     */
    public function getNumbersByCountryCode(string $code) {
        return $this->model->where('phone', 'like', '(' . $code . ')%')->get();
    }

}
