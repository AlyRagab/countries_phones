<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Description of StatesEnum
 *
 * @author ali
 */
class StatesEnum extends Enum {

    const NOT_VALID = 0;
    const VALID = 1;
    const STATES_LIST = [self::NOT_VALID, self::VALID];
    const STATES_MAPPING = ['Not Valid', 'Valid'];

}
