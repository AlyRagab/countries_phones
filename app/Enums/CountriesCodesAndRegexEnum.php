<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Description of CountriesCodesAndRegexEnum
 *
 * @author ali
 */
class CountriesCodesAndRegexEnum extends Enum {

    const CAMEROON = 'Cameroon';
    const ETHIOPIA = 'Ethiopia';
    const MOROCCO = 'Morocco';
    const MOZAMBIQUE = 'Mozambique';
    const UGANDA = 'Uganda';
    const COUNTRIES_CODES = [
        self::CAMEROON => '237',
        self::ETHIOPIA => '251',
        self::MOROCCO => '212',
        self::MOZAMBIQUE => '258',
        self::UGANDA => '256'
    ];
    const COUNTRIES_REGEX = [
        self::CAMEROON => '\(237\)\ ?[2368]\d{7,8}$',
        self::ETHIOPIA => '\(251\)\ ?[1-59]\d{8}$',
        self::MOROCCO => '\(212\)\ ?[5-9]\d{8}$',
        self::MOZAMBIQUE => '\(258\)\ ?[28]\d{7,8}$',
        self::UGANDA => '\(256\)\ ?\d{9}$'
    ];
    const COUNTRIES_LIST = [
        self::CAMEROON,
        self::ETHIOPIA,
        self::MOROCCO,
        self::MOZAMBIQUE,
        self::UGANDA
    ];

}
