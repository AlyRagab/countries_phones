<html>
    <head>
        <title>Countries Phones</title>
        <style type="text/css">
            .my-active span{
                background-color: #5cb85c !important;
                color: white !important;
                border-color: #5cb85c !important;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <body>
        <div class="container">
            @if($errors->any())
            @foreach ($errors->all() as $error)
            {{ $error }}
            <br/>
            @endforeach
            @endif
            <h2>Countries Phones</h2>
            <div>
                {{ Form::open(array('url' => route('countries-phones-index'),'method' => 'GET')) }}
                Country: {!! Form::select('country',$form_data['countries'] , request('country')); !!}
                <br/>
                State: {!! Form::select('state', $form_data['states'], request('state')); !!}
                <br/>
                {{Form::submit('Click Me!')}}
                {{ Form::close() }}
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">

                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Country</th>
                                    <th>State</th>
                                    <th>Country Code</th>
                                    <th>Phone Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($phones as $phone)
                                <tr>
                                    <td>{{$phone['country']}}</td>
                                    <td>{{$phone['validity']}}</td>
                                    <td>{{'+' . $phone['code']}}</td>
                                    <td>{{$phone['phone_number']}}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $phones->appends(request()->query())->links('custom.pagination') }}
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

